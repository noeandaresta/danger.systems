require 'middleman-gh-pages'
require 'json'
require 'bundler'
require 'open-uri'
require 'pygments'
require 'base64'
require 'rest-client'

js_tasks = %w[setup_js setup_js_logo setup_monaco setup_js_plugins]
ruby_tasks = %w[plugins_core plugins_external ruby_plugins_readme_download grab_dangerfiles getting_started_docs]

desc 'Generates all the static resources necessary to run the site.'
task generate: (js_tasks + ruby_tasks).map { |task| 'generator:' + task } do
  # Your Gemfile.lock tends to get put out of sync after some of the commands.
  puts 'Shippit'
end

desc 'Things that generate JSON'
namespace :generator do
  desc 'Generates a JSON file that represents the core plugins documentation'
  task :plugins_core do
    # Make sure we have a folder for json_data, it's not in git.
    Dir.mkdir('static/json_data') unless Dir.exist?('static/json_data')

    # Grab the Danger gem, pull out the file paths for the core plugins
    danger_gem = Gem::Specification.find_by_name 'danger'
    danger_core_plugins = Dir.glob(danger_gem.gem_dir + '/lib/danger/danger_core/plugins/*.rb')

    # Document them, move them into a nice JSON file
    output = `bundle exec danger plugins json #{danger_core_plugins.join(' ')}`
    abort('Could not generate the core plugin metadata') if output.empty?
    File.write('static/json_data/core.json', output)
    puts 'Generated core API metadata'
  end

  desc 'Generates a JSON file that represents the external plugins documentation'
  task :plugins_external do
    plugins = JSON.parse(File.read('plugins.json'))
    gems = plugins.map { |plugin| JSON.parse(open("https://rubygems.org/api/v1/gems/#{plugin}.json").read) }

    File.write('static/source/ruby/plugins.json', gems.to_json)
    puts 'Generated ruby plugin metadata'
  end

  desc 'Generates a JSON file that represents the external plugins documentation'
  task :ruby_plugins_readme_download do
    plugins = JSON.parse(File.read('static/source/ruby/plugins.json'))
    plugins_with_readme = []
    plugins.each do |plugin|
      repo = [plugin['homepage_uri'], plugin['source_code_uri']].first { |uri| uri.include? 'github' }
      puts "Skipping: #{plugin['name']} as we couldn't figure the repo" unless repo
      next unless repo
      api_url = repo.gsub('https://github.com/', 'https://api.github.com/repos/') + '/readme'
      headers = ENV.key?('DANGER_GITHUB_API_TOKEN') ? { Authorization: "token #{ENV['DANGER_GITHUB_API_TOKEN']}" } : {}
      begin
        readme = RestClient.get(api_url, headers)
        plugin['readme'] = JSON.parse(readme)
        plugins_with_readme << plugin
      rescue RestClient::ExceptionWithResponse => e
        if e.http_code != 404
          puts 'It is likely that you have hit GitHub\'s API limit'
          puts 'token value'
          puts 'Try creating an API token at https://github.com/settings/tokens'
          puts 'and set the environment variable DANGER_GITHUB_API_TOKEN to the'
          puts ''
          puts 'URL: ' + api_url
          puts 'DANGER_GITHUB_API_TOKEN: ' + ENV['DANGER_GITHUB_API_TOKEN']
          puts 
        end
      end
    end

    File.write('static/source/ruby/plugins-readmes.json', plugins_with_readme.to_json)
    puts 'Generated ruby plugins with readme metadata'
  end

  desc 'Generates a JSON file that represents the external plugins documentation'
  task :grab_dangerfiles do
    # Grab our Dangerfile plugins list
    dangerfile_repos = JSON.parse(File.read('example_oss_dangerfiles.json'))
    dangerfile_repos.each do |repo|
      branch = repo.include?('#') ? repo.split('#').last : 'master'
      repo = repo.split('#').first

      dangerfile = open("https://raw.githubusercontent.com/#{repo}/#{branch}/Dangerfile").read

      path = "static/source/dangerfiles/#{repo.tr('/', '_')}.html"
      html = Pygments.highlight(dangerfile, lexer: 'ruby', options: { encoding: 'utf-8' })

      File.write(path, html)
    end
    puts 'Downloaded Dangerfiles to `static/source/dangerfiles`'
  end

  desc 'Generate the getting started guides metadata from Danger'
  task :getting_started_docs do
    `bundle exec danger systems ci_docs > static/json_data/ci_docs.json`
    puts 'Generated getting started CI documentation'
  end

  desc 'Generate the JSON + Images necessary for the Danger JS logo to change per-deploy'
  task :setup_js_logo do
    sh 'yarn install' unless Dir.exist? 'node_modules'

    require 'twitter'
    require 'open-uri'

    def get_tweet(username)
      # This is for an account @123123wqeqweqqwe
      # so I'm not worried if people try break in.

      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = 'EYRutegHyV4G9jpv1a1QoI4lf'
        config.consumer_secret     = 'GQlYCvHyLQx8N0qbxZMzUKp7T9r4PoiqJS5RC2r5Y7aMKyEasG'
        config.access_token        = '3330338902-ismc4FmfWPK3tAjqgs9AtOk0ehR59fGqSDEGAUO'
        config.access_token_secret = 'NTtMdvqPknqUtnYPPtn2l4xnTH294qxqbgDheKtGscYzM'
      end

      timeline = client.user_timeline(username, include_rts: false, count: 25)

      saved = 0
      timeline.each do |tweet|
        next if saved > 3
        last_image = tweet.media.first.media_url
        begin
          `curl -o static/source/images/js/#{username}_original_#{saved}.png #{last_image}`
          File.write("static/source/images/js/#{username}_tweet_#{saved}.json", tweet.attrs.to_json)

          oembed = open("https://publish.twitter.com/oembed?url=https://twitter.com/#{username}/status/#{tweet.id}").read
          File.write("static/source/images/js/#{username}_tweet_oembed_#{saved}.json", oembed)

          saved += 1
        rescue
        end
      end
    end

    get_tweet('cloudyconway')
    get_tweet('crookedcosmos')

    sh 'yarn run palette'
    sh 'yarn run shrink'
  end

  desc 'Sets up the required bits for the inline monaco editor'
  task :setup_monaco do
    sh 'yarn install' unless Dir.exist? 'node_modules'
    sh 'mkdir static/source/javascripts/monaco' unless Dir.exist? 'static/source/javascripts/monaco'
    sh 'cp -r node_modules/monaco-editor/* static/source/javascripts/monaco/'
  end

  desc 'Generate the getting started guides metadata from Danger'
  task :setup_js do
    `rm -rf danger-js` if Dir.exist? 'danger-js'
    `git clone https://github.com/danger/danger-js`
    
    # Temporary
    # TODO: Get the NPM version and use that, or the latest tag?
    
    # Generate the JSON representation of the DSL
    Dir.chdir('danger-js') do
      # sh 'git checkout azz-bitbucket-server'
      sh 'yarn install'
      sh 'yarn docs'
    end

    # Migrate the API reference JSON
    sh 'mkdir -p static/json_data'
    sh 'cp danger-js/docs/js_ref_dsl_docs.json static/json_data/.'

    reference = JSON.parse(File.read('static/json_data/js_ref_dsl_docs.json'))
    sources = reference['children'].select { |c| c['name'].include? 'ci_source' }.map { |c| c['children'] && c['children'].length && c['children'][0] }.compact
    classes = sources.select { |d| d['implementedTypes'] && d['implementedTypes'].length && d['implementedTypes'][0]['name'] == 'CISource' }
    classes = classes.reject { |c| c['name'] == 'FakeCI' }
    File.write('static/json_data/js_ci_sources.json', classes.to_json)

    dsl = reference["children"].find { |c| c["name"].include? "danger.d" }["children"]
    File.write('static/json_data/js_dsl.json', dsl.to_json)

    # Migrate the docs into the static site generator
    sh('mkdir -p static/source/js/') unless Dir.exist? 'static/source/js/'
    sh 'cp -r danger-js/docs/* static/source/js/'
    changelog = File.read('danger-js/CHANGELOG.md').split("\n").reject { |l| l.start_with?('//') }.join("\n")
    File.write('static/source/js/changelog.html.md', %(---
title: CHANGELOG
subtitle: Frequently Changed Logs
layout: guide_js
order: 0
blurb: What's been changing in the codebase.
---
#{changelog}
))

    sh 'rm -rf danger-js/docs/doc_generate/'

    puts 'Generated the JS side'
  end

  desc 'Generate plugins for Danger JS'
  task :setup_js_plugins do
    sh 'yarn run plugins'
  end
end

desc 'Runs the site locally'
task :serve do
  puts 'Running locally at http://localhost:4567'
  sh 'open http://localhost:4567'
  Dir.chdir('static') do
    sh 'bundle exec middleman server'
  end
end
